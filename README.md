# fuzz-macos #

Commandline app for fuzzy testing of GUI app on macOS.

### What is this repository for? ###

* original_fuzz-macos is old PowerPC fuzzy app taken from (ftp://ftp.cs.wisc.edu/paradyn/fuzz/, http://pages.cs.wisc.edu/~bart/fuzz/)
* Copyright (c) 1989 Lars Fredriksen, Bryan So, Barton Miller
* Copyright (c) 2005 Greg Cooksey, Fred Moore, Barton Miller
* All rights reserved 



### Contribution guidelines ###

* No pull request or bug reporting, please. This code is for storage only.
* macOS-specific code is obsolete from ~macOS 10.9, so it can be unusabe in future versions.
* Apple devs keep eye on security, so functions are obsole for security reasons.
* Over all it can be still built and launched on macOS 10.15 & Xcode 11.3.

### Commandline help ###
```

./fuzz-macos -h
fuzz-aqua sends random user input to the specified application.

Usage: fuzz-aqua [-vlwch] [-s seed] [-k keys] [-d delay]
                 [-r infile] [-o outfile] PID [n]

Options:
   PID      ID of process to test; run proclist to see list of PIDs
   n        number of events to send
  -v        allow invalid user events
  -l        allow overlapping events
  -w        print window geometry (x1, y1, width, height); only works for
            single-window applications
  -s seed   seed random-number generator with <seed> (for repeatability)
  -k keys   suppress the keys represented in <keys> from being sent with Cmd
  -d delay  delay for <delay> seconds between events
  -r file   replay events stored in <file>
  -o file   save generated events to <file> for later replay
  -c        print copyright information and exit
  -h        print this help and exit

Default:    fuzz-aqua -d 0 -k "QMEsc" <PID> 1000

For a more detailed explanation of the options, see fuzz-aqua.m
```